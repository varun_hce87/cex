This project is created in Yii2

I have used Yii2 advanced template to create frontend and api along with common and command line console.
Backend for admin panel is also there but that is not being used in this project so we can leave it out for now..

Please follow the below steps to run this project locally

1) Please use PHP 5.5+ till PHP7 and nginx server with mysql
2) Please create database in mysql with the name of cex and assign user with root and password with 1230 and grant all the previlidges to this user for this database.
3) Please use command php yii migrate inside /var/www/html/cex/server, this will create a user table in database...
4) Please create vhost in nginx for api and frontend
API URL is http://api.cex.dev
Fronend URL is http://cex.dev
Also please add both the line mentioned below in /etc/hosts
127.0.0.1	cex.dev
127.0.0.1	api.cex.dev

I have also attached these nginx vhost files just for your help so that you can run this project without any problem.

If the steps mentioned above will be followed completely then I don't think you guys will encounter any problem running this project