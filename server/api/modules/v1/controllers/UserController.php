<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * User Controller API
 *
 * @author Varun Gupta <varun_hce87@yahoo.co.in>
 */
class UserController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\User';

    public function actionImage($id)
    {
	$model = $this->modelClass::findOne($id);
	$oldImage = $model->profile_pic;
	$file = $_FILES['profile_pic'];
	$_FILES = ['User' => [
	    'name' => ['profile_pic' => $file['name']],
	    'type' => ['profile_pic' => $file['type']],
	    'tmp_name' => ['profile_pic' => $file['tmp_name']],
	    'error' => ['profile_pic' => $file['error']],
	    'size' => ['profile_pic' => $file['size']],
	]];
	$image = UploadedFile::getInstance($model, 'profile_pic');
	$model->profile_pic = $image->basename. '.'. $image->extension;
	if($model->save()) {
	    $model->profile_pic = $image;
	    if($model->upload()) {
		$model->deleteImage($oldImage);
		return ['message' => "User profile image has been updated successfully"];
	    }
	}
    }

    public function actionSearch()
    {
	$model = new $this->modelClass;
	if(\Yii::$app->request->get('id')) {
	    $ids = json_decode(\Yii::$app->request->get('id'));
	    try {
		$provider = new ActiveDataProvider([
		    'query' => $model->find()->where(['id' => $ids]),
		    'pagination' => false,
		]);
            } catch(Exception $ex) {
		throw new \yii\web\HttpException(500, 'Internal Server Error');
	    }

	    if($provider->getCount() <= 0) {
		throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
		return $provider;
	    }
        }
    } 
}
