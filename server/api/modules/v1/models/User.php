<?php
namespace api\modules\v1\models;

use yii;
use \yii\db\ActiveRecord;
use \yii\web\UploadedFile;

/**
 * User Model
 *
 * @author Varun Gupta <varun_hce87@yahoo.co.in>
 */
class User extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'user';
	}
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'required'],
	    [['profile_pic'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
	    [['profile_pic'], 'default', 'value'=>""]
        ];
    }

    public function upload()
    {
        if($this->validate()) {
	    $uploads = Yii::getAlias('@uploads');
            $this->profile_pic->saveAs($uploads.'/'.$this->profile_pic->basename.'.'.$this->profile_pic->extension);
	    return true;
        } else {
            return false;
        }
    }

    public function deleteImage($oldImage = '')
    {
	if($oldImage != '' && file_exists("uploads/".$oldImage))
	    unlink("uploads/".$oldImage);
    }

    public function getNext()
    {
	
    }
}
