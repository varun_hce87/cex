<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    private $modelClass = '\api\modules\v1\models\User';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	    CURLOPT_URL => "http://api.cex.dev/v1/user",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
	    ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	    echo "cURL Error #:" . $err;
	} else {
	    $res = json_decode($response);
	}
        return $this->render('index', ['res' => $res]);
    }

    /**
     * Add a profile
     *
     * @return mixed
     */
    public function actionAddProfile()
    {
	$model = new $this->modelClass;
        return $this->render('profile-form', ['model' => $model, 'action' => 'Create']);
    }

    /**
     * Edit a profile.
     *
     * @return mixed
     */
    public function actionEditProfile($id)
    {
	$model = $this->modelClass::findOne($id);
        return $this->render('profile-form', ['model' => $model, 'action' => 'Edit']);
    }

    /**
     * Displays Profile.
     *
     * @return mixed
     */
    public function actionProfile($id)
    {
	$model = $this->modelClass::findOne($id);
        return $this->render('profile', ['model' => $model]);
    }

    /**
     * Show all profiles with pagination.
     *
     * @return mixed
     */
     public function actionDisplay()
     {
	$query = $this->modelClass::find();
	$count = $query->count();
	$pagination = new \yii\data\Pagination(['totalCount' => $count, 'pageSize' => 1]);
	//var_dump($pagination);die;
	$users = $query->offset($pagination->offset)->limit($pagination->limit)->all();
	return $this->render('display', ['users' => $users, 'pagination' => $pagination]);
     }
}
