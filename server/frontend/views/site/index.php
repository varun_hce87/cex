<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'CEX Quiz';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Users Profile Listing</h1>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['/add-profile']) ?>">Add Profile</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>S No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
		    <?php foreach($res as $i => $val): ?>
		    <tr>
			<td><?= $i+1 ?></td>
			<td><?= $val->name ?></td>
			<td><?= $val->email ?></td>
			<td><?= $val->phone ?></td>
			<td><a href="<?= Url::to(['/edit-profile/'.$val->id]) ?>">Edit</a> | <a href="<?= Url::to(['/profile/'.$val->id]) ?>">View</a></td>
		    </tr>
		    <?php endforeach; ?>
		</tbody>
            </table>
        </div>
    </div>
</div>
