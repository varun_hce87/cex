<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $action.' Profile';
$this->params['breadcrumbs'][] = $this->title;
$request = ($action == "Create") ? "POST" : "PUT";
$url = ($request == "POST") ? Yii::$app->params['apiUrl'].'user' : Yii::$app->params['apiUrl'].'user/'.$model->id;
$js = '
$("body").on("submit", "form#form-profile", function(e) {
e.preventDefault();
var req = "'.$request.'";
var form = $(this);
var fd = new FormData();
var file_data = $("#user-profile_pic")[0].files;
if(!file_data.length && req == "POST") {
$("#user-profile_pic").closest("div").removeClass("has-success").addClass("has-error");
$("#user-profile_pic").next().text("Profile pic is a required field");
}
if($("#user-name").val() == "") {
$("#user-name").closest("div").removeClass("has-success").addClass("has-error");
$("#user-name").next().text("Name cannot be blank");
}
if($("#user-phone").val() == "") {
$("#user-phone").closest("div").removeClass("has-success").addClass("has-error");
$("#user-phone").next().text("Phone cannot be blank");
}
if($("#user-email").val() == "") {
$("#user-email").closest("div").removeClass("has-success").addClass("has-error");
$("#user-email").next().text("Email cannot be blank");
}
if(form.find(".has-error").length) {
return false;
}
var Data = JSON.stringify({"name": $("#user-name").val(), "phone": $("#user-phone").val(), "email": $("#user-email").val()});
$.ajax({
url: "'.$url.'",
method: req,
headers:{
"content-type": "application/json",
},
crossDomain: true,
data: Data,
error: function(data){
var errors = data.responseJSON;
console.log(errors);
// Render the errors with js ...
}
}).done(function(response) {
if(response["id"] && file_data.length) {
fd.append("profile_pic", file_data[0]);
$.ajax({
url: "'.Yii::$app->params['apiUrl'].'user/"+response["id"]+"/image",
method: "POST",
crossDomain: true,
contentType: false,
processData: false,
mimeType: "multipart/form-data",
data: fd,
error: function(data){
var errors = data.responseJSON;
console.log(errors);
}
}).done(function(response) {
console.log(response);
});
}
window.location.href = "'.Yii::$app->homeUrl.'";
});
});
';
$this->registerJs($js, View::POS_END);
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to <?= strtolower($action) ?> profile:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-profile', 'enableClientValidation' => false]); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'phone') ?>
		<?= ($model->profile_pic != '') ? '<img src="/uploads/'.$model->profile_pic.'" width="120" />' : '' ?>
		<?= $form->field($model, 'profile_pic')->fileInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($action.' Profile', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
