<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Display Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php foreach($users as $model): ?>
    <div class="row">
        <div class="col-lg-3">
	    <dt>Name</dt>
        </div>
	<div class="col-lg-9">
	    <dd><?= $model->name ?></dd>
	</div>
	<div class="col-lg-3">
            <dt>Phone</dt>
        </div>
        <div class="col-lg-9">
            <dd><?= $model->phone ?></dd>
        </div>
	<div class="col-lg-3">
            <dt>Email</dt>
        </div>
        <div class="col-lg-9">
            <dd><?= $model->email ?></dd>
        </div>
	<div class="col-lg-3">
            <dt>Profile Pic</dt>
        </div>
        <div class="col-lg-9">
            <dd><img src="/uploads/<?= $model->profile_pic ?>" width="120" /></dd>
        </div>
    </div>
    <?php endforeach; ?>
    <div class="row">
	<div class="col-lg-12">
	    <?= LinkPager::widget(['pagination' => $pagination]) ?>
	</div>
    </div>
</div>
